<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKliniksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kliniks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('alamat');
            $table->string('provinsi');
            $table->string('kotakab');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('kontak');
            $table->string('email')->nullable();
            $table->string('lat');
            $table->string('long');
            $table->string('user_id');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('tipe');
            $table->string('logo')->nullable();
            $table->longText('foto')->nullable();
            $table->mediumText('links')->nullable();
            $table->mediumText('jam_hari_kerja')->nullable();
            $table->tinyInteger('max_keterlambatan')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kliniks');
    }
}
