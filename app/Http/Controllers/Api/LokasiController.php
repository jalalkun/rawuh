<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Provinsi;
use App\Models\Kotakab;
use App\Models\Kecamatan;
use App\Models\Kelurahan;

class LokasiController extends Controller
{
    function getProvinsi(){
        $provinsis = Provinsi::orderBy("name")->get();
        return response()->json([
            "status"    => 200,
            "message"   => "Sukses",
            "data"      => json_decode($provinsis)
        ]);
    }

    function getKotaKab(Request $request){
        $provinsiid = $request->provinsiid;
        $kotakab = Kotakab::where('provinsi_id', $provinsiid)->orderBy("name")->get();
        return response()->json([
            "status"    => 200,
            "message"   => "Sukses",
            "data"      => json_decode($kotakab)
        ]);
    }

    function getKecamatan(Request $request){
        $kotakabid = $request->kotakabid;
        $kecamatan = Kecamatan::where('kotakab_id', $kotakabid)->orderBy("name")->get();
        return response()->json([
            "status"    => 200,
            "message"   => "Sukses",
            "data"      => json_decode($kecamatan)
        ]);
    }

    function getKelurahan(Request $request){
        $kecamatanid = $request->kecamatanid;
        $kelurahan = Kelurahan::where('kecamatan_id', $kecamatanid)->orderBy("name")->get();
        return response()->json([
            "status"    => 200,
            "message"   => "Sukses",
            "data"      => json_decode($kelurahan)
        ]);
    }
}
