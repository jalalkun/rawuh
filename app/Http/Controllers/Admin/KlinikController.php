<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Klinik;

class KlinikController extends Controller
{
    public function kliniksAll(){
        $kliniks = Klinik::all();
        return view('administrator.dashboard.kliniks.kliniks', compact('kliniks'));
    }

    public function klinikDetail(Request $request){
        $klinik = Klinik::findOrFail($request->id);
        return view('administrator.dashboard.kliniks.itemKlinikDetail', compact('klinik'));
    }
}
