<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function indexList(){
        $users = User::where('tipe', '!=' , '0')->paginate(50);
        $users->withPath('/users/list');
        // Log::debug("index list " . json_encode($users));
        $users = json_encode($users);
        $users = json_decode($users);
        return view('administrator.dashboard.user.list', compact('users'));
    }
}
