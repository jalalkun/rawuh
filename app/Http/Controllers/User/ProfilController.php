<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Klinik;

class ProfilController extends Controller
{
    public function profilSaya(){
        $listKlinik = Klinik::where('user_id', Auth::user()->id)->get();
        return view('user.profil.profilsaya', compact('listKlinik'));
    }

    public function perbaruiProfil(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $user = Auth::user();
        $user->name = $request->name;
        $user->no_hp = $request->no_hp;
        $user->alamat = $request->alamat;
        if($user->save()){
            $request->session()->flash('perbaruiprofil.sukses', 'Profil anda berhasil diperbarui');
            return redirect()->route('profil');
        }else{
            Log::debug('gagal save');
            $request->session()->flash('perbaruiprofil.gagal', 'Profil anda gagal diperbarui');
            return redirect()->route('profil');
        }
    }
}
