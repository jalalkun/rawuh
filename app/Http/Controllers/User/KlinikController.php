<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Klinik;
use App\Models\RequestKlinik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Provinsi;

class KlinikController extends Controller
{
    public function klinikPage(){
        $listKlinik = Klinik::where('user_id', Auth::user()->id)->get();
        return view('user.dashboard.klinik.kliniks', compact('listKlinik'));
    }

    public function formPage(){
        $provinsis = Provinsi::orderBy("name")->get();
        return view('user.dashboard.klinik.form-klinik', compact('provinsis'));
    }

    public function store(Request $request){
        $request->validate([
            "klinikname" => "required|string",
            "tipe" => "required|in:1,2",
            "alamat" => "required|string",
            "lat" => "required",
            "long" => "required",
            "provinsi" => "required|not_in:0",
            "kotakab" => "required|not_in:0",
            "kecamatan" => "required|not_in:0",
            "kelurahan" => "required|not_in:0"
        ]);
        $id = Auth::user()->id;
        $klinik = new Klinik();
        $klinik->name = $request->klinikname;
        $klinik->tipe = $request->tipe;
        $klinik->alamat = $request->alamat;
        $klinik->lat = $request->lat;
        $klinik->long = $request->long;
        $klinik->user_id = "$id";
        $klinik->tipe = $request->tipe;
        $klinik->provinsi = $request->provinsi;
        $klinik->kotakab = $request->kotakab;
        $klinik->kecamatan = $request->kecamatan;
        $klinik->kelurahan = $request->kelurahan;
        $hp = explode(",", $request->no_hp);
        $tel = explode(",", $request->telepon);
        $mail = explode(",", $request->email);
        $wa = explode(",", $request->whatsapp);
        $tele = explode(",", $request->telegram);
        $kontak = array();
        array_push($kontak,[
            "no_hp" => $hp,
            "telepon" => $tel,
            "whatsapp" => $wa,
            "telegram" => $tele
        ]);
        $kontak = $kontak[0];
        $kontak = json_encode($kontak);
        $klinik->kontak = $kontak;
        $email = array();
        array_push($email, [
            "email" => $mail
        ]);
        $email = $email[0];
        $email = json_encode($email);
        $klinik->email = $email;
        $facebook = $request->facebook;
        $instagram = $request->instagram;
        $twitter = $request->twitter;
        $links = array();
        array_push($links, [
            "facebook" => $facebook,
            "instagram" => $instagram,
            "twitter" => $twitter
        ]);
        $links = $links[0];
        $links = json_encode($links);
        $klinik->links = $links;
        if ($klinik->save()) {
            $requestKlinik = new RequestKlinik();
            $requestKlinik->klinik_id = $klinik->id;
            if($requestKlinik->save()){
                $request->session()->flash('klinik-request.sukses', 'Pengajuan Klinik Berhasil, tunggu hingga admin melakukan konfirmasi');
            }else{
                $request->session()->flash('klinik-request', 'Pengajuan Klinik Gagal, silahkan cek kembali detail klinik anda, atau hubungi admin');
            }
            return redirect()->route('user.kliniks');
        }else{
            $request->session()->flash('klinik-request', 'Pengajuan Klinik Gagal, silahkan cek kembali detail klinik anda, atau hubungi admin');
            return redirect()->route('user.kliniks');
        }
    }
}
