<?php
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

// Tipe user
if(! function_exists('typeUser')){
    function typeUser($n){
        switch ($n){
            case 0 :
                return 'Admin';
                break;
            case 1 :
                return 'Pemilik';
                break;
        }
    }
}

if (! function_exists('klinikTipe')) {
    function klinikTipe($n){
        switch ($n) {
            case 1:
                return 'Pusat';
                break;
            case 2:
                return 'Cabang';
                break;
        }
    }
}

// klinik status
if (! function_exists('klinikStatus')) {
    function klinikStatus($n){
        switch ($n) {
            case 0:
                return 'Request';
                break;
            case 1:
                return 'Aktif';
                break;
            case 2:
                return 'Tidak Aktif';
                break;
            case 3:
                return 'Dihapus';
                break;
        }
    }
}

if (! function_exists('areActiveRoutes')) {
    function areActiveRoutes(Array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

?>
