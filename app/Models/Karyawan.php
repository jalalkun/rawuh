<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    public function klinik(){
        return $this->belongsTo('App\Models\Klinik', 'klinik_id', 'id');
    }

    public function kerja(){
        return $this->belongsTo('App\Models\Kerja', 'kerja_id', 'id');
    }
}
