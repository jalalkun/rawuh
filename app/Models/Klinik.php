<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Klinik extends Model
{
    use HasFactory;

    public function pemilik(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
