<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\LokasiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('provinsi', [LokasiController::class, 'getProvinsi'])->name('api.provinsi');
Route::get('kotakab', [LokasiController::class, 'getKotaKab'])->name('api.kotakab');
Route::get('kecamatan', [LokasiController::class, 'getKecamatan'])->name('api.kecamatan');
Route::get('kelurahan', [LokasiController::class, 'getKelurahan'])->name('api.kelurahan');
