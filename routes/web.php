<?php

use Illuminate\Support\Facades\Route;
// admin
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AuthController as AuthAdmin;
use App\Http\Controllers\Admin\KlinikController as AdminKlinik;

// user
use App\Http\Controllers\User\AuthController as AuthUser;
use App\Http\Controllers\User\ProfilController;
use App\Http\Controllers\User\DashboardController as UserDashboard;
use App\Http\Controllers\User\KlinikController as UserKlinik;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

// admin
Route::prefix('admin')->group(function () {
   Route::get('mlebet', [AuthAdmin::class, 'loginPage'])->name('admin.login');
   Route::post('mlebet', [AuthAdmin::class, 'login'])->name('admin.post.login');
});
Route::middleware(['admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('dashboard', function () {
            return view('administrator.dashboard.dashboard');
        })->name('admin.dashboard');
        Route::get('users/list', [UserController::class, 'indexList'])->name('list_user');
        Route::post('logout', [AuthAdmin::class, 'logout'])->name('admin.logout');
        Route::get('kliniks', [AdminKlinik::class, 'kliniksAll'])->name('admin.kliniks');
        Route::get('kliniks/details', [AdminKlinik::class, 'klinikDetail'])->name('admin.klinik.detail.popup');
    });
});

// users
Route::get('register', [AuthUser::class, 'registerPage'])->name('register-page');
Route::post('register', [AuthUser::class, 'storeData'])->name('mendaftar');
Route::get('login', [AuthUser::class, 'loginPage'])->name('login');
Route::post('login', [AuthUser::class, 'authenticate']);

Route::middleware(['auth'])->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('dashboard', [UserDashboard::class, 'dashboardPage'])->name('user.dashboard');
        Route::get('logout', [AuthUser::class, 'logout'])->name('user.logout');
        Route::get('profil', [ProfilController::class, 'profilSaya'])->name('profil');
        Route::post('profil/simpan', [ProfilController::class, 'perbaruiProfil'])->name('perbarui-profil');
        Route::get('form-klinik', [UserKlinik::class, 'formPage'])->name('form.klinik');
        Route::post('form-klinik', [UserKlinik::class, 'store'])->name('store.klinik');
        Route::get('kliniks', [UserKlinik::class, 'klinikPage'])->name('user.kliniks');
     });
});



