@extends('user.layouts.sidenav')
@section('content.user')
@if ($message = Session::get('login.sukses'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{$message}}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
    Dashboard user {{Auth::user()->name}}
@endsection
