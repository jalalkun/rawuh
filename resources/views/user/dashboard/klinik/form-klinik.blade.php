@extends('user.layouts.sidenav')
@section('title_page')
    Form Klinik
@endsection
@section('head.script')
<style type="text/css">
    /* Set the size of the div element that contains the map */
    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }
  </style>

<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        const uluru = { lat: -7.322816, lng: 110.480461 };
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 9,
            center: uluru,
        });
        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
            position: uluru,
            draggable:true,
        });

        google.maps.event.addListener(marker, 'dragend', function (evt) {
            console.log('lat ' + evt.latLng.lat().toFixed(6));
            console.log('long ' + evt.latLng.lng().toFixed(6));
            document.getElementById("lat").value = evt.latLng.lat().toFixed(6);
            document.getElementById("long").value = evt.latLng.lng().toFixed(6);
            // $("#txtLat").val(evt.latLng.lat().toFixed(6));
            // $("#txtLng").val(evt.latLng.lng().toFixed(6));
            map.panTo(evt.latLng);
        });

        marker.setMap(map);
    }
  </script>

@endsection
@section('content.user')
<div class="container-fluid">
    @include('user.layouts.errormessage')
    <div class="container-md">
        <div>
            <h2>
                <strong>Form Klinik</strong>
            </h2>
        </div>
        <div class="mt-2">
            <form action="form-klinik" method="POST">
                @csrf
                {{-- pofil klinik --}}
                <div class="card">
                    <div class="card-header">
                        Profil Klinik
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            <label for="klinikname" class="form-label">Nama <small>*</small></label>
                            <input type="text" class="form-control" id="klinikname" name="klinikname" placeholder="Nama Klinik" required>
                        </div>
                        <div class="mb-2">
                            <label for="tipe" class="form-label">Tipe <small>*</small></label>
                            <select class="form-select" id="tipe" name="tipe" aria-label="Default select example" required>
                                <option selected>Pilih Tipe</option>
                                <option value="1">Pusat</option>
                                <option value="2">Cabang</option>
                            </select>
                        </div>
                    </div>
                </div>
                {{-- contact --}}
                <div class="card mt-3">
                    <div class="card-header">
                        Kontak
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            <label for="email" class="form-label">Email </label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="mb-2">
                            <label for="no_hp" class="form-label">No Hp </label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No Hp">
                        </div>
                        <div class="mb-2">
                            <label for="telepon" class="form-label">Telepon </label>
                            <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon">
                        </div>
                        <div class="mb-2">
                            <label for="whatsapp" class="form-label">Whatsapp </label>
                            <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="Whatsapp">
                        </div>
                        <div class="mb-2">
                            <label for="telegram" class="form-label">Telegram </label>
                            <input type="text" class="form-control" id="telegram" name="telegram" placeholder="Telegram">
                        </div>
                    </div>
                    <div class="card-footer">
                        <small>Jika lebih dari 1 pisahkan dengan koma</small>
                    </div>
                </div>
                {{-- sosmed --}}
                <div class="card mt-3">
                    <div class="card-header">
                        Sosial Media
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            <label for="facebook" class="form-label">Facebook </label>
                            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook">
                        </div>
                        <div class="mb-2">
                            <label for="instagram" class="form-label">Instagram </label>
                            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram">
                        </div>
                        <div class="mb-2">
                            <label for="twitter" class="form-label">Twitter </label>
                            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter">
                        </div>
                    </div>
                    <div class="card-footer">
                        <small>hanya username saja, contoh : jalal.kun. Hanya 1 akun</small>
                    </div>
                </div>
                {{-- lokasi --}}
                <div class="card mt-3">
                    <div class="card-header">
                        Lokasi
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            <label for="tipe" class="form-label">Provinsi <small>*</small></label>
                            <select class="form-select" id="provinsi" name="provinsi" aria-label="Default select example" required>
                                <option selected value="0">Pilih Provinsi</option>
                                @foreach ($provinsis as $provinsi)
                                <option value="{{$provinsi->id}}">{{$provinsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-2">
                            <label for="tipe" class="form-label">Kota atau Kabupaten <small>*</small></label>
                            <select class="form-select" id="kotakab" name="kotakab" aria-label="Default select example" required>
                                <option selected value="0">Pilih Kota atau Kabupaten</option>
                            </select>
                        </div>
                        <div class="mb-2">
                            <label for="tipe" class="form-label">Kecamatan <small>*</small></label>
                            <select class="form-select" id="kecamatan" name="kecamatan" aria-label="Default select example" required>
                                <option selected value="0">Pilih Kecamatan</option>
                            </select>
                        </div>
                        <div class="mb-2">
                            <label for="tipe" class="form-label">Kelurahan <small>*</small></label>
                            <select class="form-select" id="kelurahan" name="kelurahan" aria-label="Default select example" required>
                                <option selected value="0">Pilih Kelurahan</option>
                            </select>
                        </div>
                        <div class="mb-2">
                            <label for="alamat">Alamat <small>*</small></label>
                            <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" required>
                        </div>
                        <div class="mb-2">
                            <label for="latlong">Lokasi <small>*</small></label>
                            <div class="container-sm">
                                <div id="map"></div>
                            </div>
                            <input type="hidden" id="lat" name="lat" required>
                            <input type="hidden" id="long" name="long" required>
                        </div>
                    </div>
                    <div class="card-footer">
                        <small>Klik dan geser marker berwarna merah untuk menentukan lokasi</small>
                    </div>
                </div>
                <small>* Harus diisi</small>
                <div class="mt-3 mb-3 text-center">
                    <button type="button" class="btn btn-secondary">Kembali</button>
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script async
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_KEY')}}&callback=initMap">
</script>
<script>
    $("#provinsi").change(function(){
        var provinsiid = $('#provinsi').val();
        if(provinsiid.length > 0){
            $.get('{{ route('api.kotakab') }}', {_token:'{{ csrf_token() }}', provinsiid:provinsiid}, function(data){
                var res = JSON.parse(JSON.stringify(data));
                var kotakab = res.data;
                $('#kotakab').empty();
                $('#kotakab').append($('<option>', {
                        value: 0,
                        text: "Pilih Kota atau Kabupaten",
                        selected: 'selected'
                }));
                for (let index = 0; index < kotakab.length; index++) {
                    const element = kotakab[index];
                    $('#kotakab').append($('<option>', {
                        value: element['id'],
                        text: element['name']
                    }));
                }
            });
        }
    });
    $("#kotakab").change(function(){
        var kotakabid = $("#kotakab").val();
        if(kotakabid.length > 0){
            $.get('{{ route('api.kecamatan') }}', {_token:'{{ csrf_token() }}', kotakabid:kotakabid}, function(data){
                var res = JSON.parse(JSON.stringify(data));
                var kecamatan = res.data;
                $("#kecamatan").empty();
                $("#kecamatan").append($('<option>',{
                    value: 0,
                    text: "Pilih Kecamatan",
                    selected: "selected"
                }));
                for (let index = 0; index < kecamatan.length; index++) {
                    const element = kecamatan[index];
                    $("#kecamatan").append($('<option>', {
                        value: element['id'],
                        text: element['name']
                    }));
                }
            });
        }
    });
    $("#kecamatan").change(function(){
        var kecamatanid = $("#kecamatan").val();
        if(kecamatanid.length > 0){
            $.get('{{ route('api.kelurahan') }}', {_token:'{{ csrf_token() }}', kecamatanid:kecamatanid}, function(data){
                var res = JSON.parse(JSON.stringify(data));
                var kelurahan = res.data;
                $("#kelurahan").empty();
                $("#kelurahan").append($('<option>',{
                    value: 0,
                    text: "Pilih Kelurahan",
                    selected: "selected"
                }));
                for (let index = 0; index < kelurahan.length; index++) {
                    const element = kelurahan[index];
                    $("#kelurahan").append($('<option>', {
                        value: element['id'],
                        text: element['name']
                    }));
                }
            });
        }
    });
</script>

@endsection
