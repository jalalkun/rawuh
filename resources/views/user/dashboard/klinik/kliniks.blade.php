@extends('user.layouts.sidenav')
@section('title_page')
    Dashboard | Kliniks
@endsection
@section('content.user')

<div class="container-fluid">
    @if ($message = Session::get('klinik-request.sukses'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if ($message = Session::get('klinik-request.gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row">
        <div class="col-lg">
            <div class="card" style="margin-top: 20px;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm">
                            List Klinik
                        </div>
                        <div class="col-sm text-end">
                            <a href="{{route('form.klinik')}}">Tambah Klinik</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($listKlinik as $klinik)
                                <th scope="row">{{$loop->index+1}}</th>
                                <td>{{$klinik->name}}</td>
                                <td>{{$klinik->alamat}}</td>
                                <td>{{klinikStatus($klinik->status)}}</td>
                                <td>Aksi</td>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
