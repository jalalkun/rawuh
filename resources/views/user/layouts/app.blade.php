<!DOCTYPE html>
<head>
    <title>@yield('title_page')</title>

    <meta name="robots" content="noindex, nofollow"/>
    <meta name="googlebot" content="noindex, nofollow"/>

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.rtl.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/sidebar.css') }}" > --}}
    <script defer src="{{ asset('/js/all.min.js') }}"></script> <!--load all styles -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    @yield('style')

    @yield('head.script')

</head>

<body>
    @yield('content')

    @yield('script')

    {{-- <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.esm.min.js') }}"></script>
</body>



</html>
