@extends('user.layouts.app')
@section('style')
<style>
    body {
        display: flex;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
    }

    .form-signup {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }

    .form-signin .checkbox {
        font-weight: 400;
    }

</style>
@endsection

@section('content')
<div class="container">
    @include('user.layouts.errormessage')
    @if ($message = Session::get('pendaftaran.sukses'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <form action="register" method="POST">
        @csrf
        <main class="form-signup">
            <h1 class="h3 mb-3 fw-normal text-center">Silahkan Mendaftar</h1>
            <div class="form-floating mb-1">
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                <label for="nama">Nama</label>
            </div>
            <div class="form-floating mb-1">
                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
                <label for="email">Email</label>
            </div>
            <div class="form-floating mt-1">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                <label for="password">Password</label>
            </div>
            <small>min 6 karakter</small>
            <button class="w-100 btn btn-lg btn-primary mt-4" type="submit">Mendaftar</button>
            <div class="text-center">
                <small>atau</small>
            </div>
            <div class="text-center">
                <a href="{{route('login')}}"><small>Masuk</small></a>
            </div>
        </main>
    </form>
</div>
@endsection
