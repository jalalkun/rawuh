@extends('user.layouts.sidenav')
@section('title_page')
    Dashboard | Profil Saya
@endsection
@section('content.user')

<div class="container-fluid">
    @if ($message = Session::get('perbaruiprofil.sukses'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if ($message = Session::get('perbaruiprofil.gagal'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if ($message = Session::get('klinik-request.sukses'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        {{-- kiri --}}
        <div class="col-lg">
            {{-- Profil --}}
            <div class="card" style="margin-top: 20px;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm">
                            Profil Saya
                        </div>
                        <div class="col-sm text-end">
                            <i class="fas fa-edit" data-bs-toggle="modal" data-bs-target="#modalEditProfil"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Nama    : {{Auth::user()->name}}</li>
                        <li class="list-group-item">Email   :{{Auth::user()->email}}</li>
                        <li class="list-group-item">
                            @if (Auth::user()->alamat==null)
                                Alamat
                            @else
                                Alamat  : {{ Auth::user()->alamat}}
                            @endif
                        </li>
                        <li class="list-group-item">
                            @if (Auth::user()->no_hp==null)
                                No Hp
                            @else
                                No Hp   : {{Auth::user()->no_hp}}
                            @endif
                        </li>
                    </ul>
                </div>
            </div>


            {{-- Account --}}
            <div class="card" style="margin-top: 20px;">
                <div class="card-header">
                    Akun
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm">Tipe : {{typeUser(Auth::user()->tipe)}}</div>
                                <div class="col-sm text-end"><a href="{{route('form.klinik')}}">Jadi Pemilik</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        {{-- kanan --}}
        <div class="col-lg">
            <div class="card" style="margin-top: 20px;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm">
                            List Klinik
                        </div>
                        <div class="col-sm text-end">
                            <i class="fas fa-edit" data-bs-toggle="modal" data-bs-target="#modalEditProfil"></i>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($listKlinik as $klinik)
                                <th scope="row">{{$loop->index+1}}</th>
                                <td>{{$klinik->name}}</td>
                                <td>{{$klinik->alamat}}</td>
                                <td>{{klinikStatus($klinik->status)}}</td>
                                <td>Aksi</td>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="modalEditProfil" name="modalEditProfil"  tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('perbarui-profil') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        Edit profil
                    </div>
                    <div class="modal-body" style="width: 450px">
                        <div class="mb-1">
                            <label for="name" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->name}}" required>
                        </div>
                        <div class="mb-1">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{Auth::user()->email}}" readonly>
                        </div>
                        <div class="mb-1">
                            <label for="no_hp" class="form-label">No Hp</label>
                            <input type="tel" class="form-control" id="no_hp" name="no_hp" value="{{Auth::user()->no_hp}}" required>
                        </div>
                        <div class="mb-1">
                            <label for="alamat" class="form-label">Alamat</label>
                            <textarea rows="3" class="form-control" id="alamat" name="alamat" value="{{Auth::user()->alamat}}" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
