@extends('administrator.dashboard.sidenav')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel. {{Auth::user()->name}}</p>
@stop
