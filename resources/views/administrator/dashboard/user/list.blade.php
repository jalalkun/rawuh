@extends('administrator.dashboard.sidenav')
@section('content_header')
    <h1>List User</h1>
@stop

@section('content')
    {{-- {{Log::debug('user list ' . json_encode($users))}}
    {{Log::debug('user page ' . $users->current_page)}} --}}
    <div class="wrapper">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Tipe</th>
                </tr>
            </thead>
            <tbody>
            @if (sizeof($users->data)>0)
                @foreach ($users->data as $key => $user)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{typeUser($user->tipe)}}</td>
                    </tr>
                @endforeach
            @else
            </tbody>
        </table>
        @endif
    </div>
@stop
