@extends('administrator.dashboard.sidenav')
@section('content_header')
    <h1>List Klinik</h1>
@stop

@php
    use App\Models\Provinsi;
    use App\Models\Kotakab;
    use App\Models\Kecamatan;
    use App\Models\Kelurahan;
@endphp

@section('content')
    <div class="wrapper">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Pemilik</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
            @if (sizeof($kliniks)>0)
                @foreach ($kliniks as $klinik)
                @php
                    $provinsi = Provinsi::find($klinik->provinsi);
                    $kotakab = Kotakab::find($klinik->kotakab);
                    $kecamatan = Kecamatan::find($klinik->kecamatan);
                    $kelurahan  = Kelurahan::find($klinik->kelurahan);
                @endphp
                    <tr>
                        <th scope="row">{{$loop->index+1}}</th>
                        <td>{{$klinik->name}}</td>
                        <td>{{$klinik->alamat}}, <br> {{$kelurahan->name}}, {{$kecamatan->name}}, {{$kotakab->name}}, {{$provinsi->name}}</td>
                        <td>{{$klinik->pemilik->name}}</td>
                        <td>{{klinikStatus($klinik->status)}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Dropdown button
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button onclick="" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#klinik_detail">Detail</button>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
            </tbody>
        </table>
        @endif
    </div>

    <div class="modal fade" id="klinik_detail" name="klinik_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="c-preloader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <div id="klinik-detail-modal-body">

                </div>
            </div>
        </div>
    </div>
@stop
