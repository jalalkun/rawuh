<!DOCTYPE html>
<head>
    <title>@yield('title_page')</title>

    <meta name="robots" content="noindex, nofollow"/>
    <meta name="googlebot" content="noindex, nofollow"/>

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.rtl.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/sidebar.css') }}" class=""> --}}
    <script defer src="{{ asset('/js/all.min.js') }}"></script> <!--load all styles -->

    @yield('style')

</head>

<body>
    @yield('content')




    <script>
        function show_klinik_details(id){
            $('#klinik-detail-modal-body').html(null);

            if(!$('#modal-size').hasClass('modal-lg')){
                $('#modal-size').addClass('modal-lg');
            }

            $.post('{{ route('admin.klinik.detail.popup') }}', { _token : '{{ @csrf_token() }}', id:id}, function(data){
                $('#klinik-detail-modal-body').html(data);
                $('#klinik_detail').modal();
                $('.c-preloader').hide();
            });
        }
    </script>

    @yield('script')

    {{-- <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.esm.min.js') }}"></script>
</body>
</html>
